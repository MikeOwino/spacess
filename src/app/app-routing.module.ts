import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashComponent } from './dash/dash.component';
import { PostAddComponent } from './post-add/post-add.component';
import { PostAdComponent } from './post-ad/post-ad.component';

const routes: Routes = [
  // {
  //   path: '/dash',
  //   component: DashComponent
  // },
  // {
  //   path: '/post-add',
  //   component: PostAddComponent
  // },
  // {
  //   path: '/post-ad',
  //   component: PostAdComponent
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: [
    DashComponent,
    PostAddComponent,
    PostAdComponent
  ]
})
export class AppRoutingModule { }
